#ifndef _DEFS_H_
#define _DEFS_H_

// Macros and definitions go here:

#define BUFFER 100 /* Number of memorized frequencies used to calculate mean frequency */

/* Structure defining moisture sensor */
typedef struct {
	TIM_HandleTypeDef *htim; 	/* Pointer to auto-generated timer instance */
	uint32_t channel; 			/* Timer channel */
	volatile uint32_t start;	/* "Timestamp" for first detected edge */
	volatile uint32_t end;		/* "Timestamp" for second detected edge */
	int first_edge; 			/* Stores start for start and end */
	uint32_t freqs[BUFFER];		/* Array for storing frequencies in order to calculate a mean value */
	uint32_t mean_freq; 		/* See above */
	uint32_t counter;			/* Counter variable for calculating mean (see implementation of measure_frequency()) */
} Sensor;

#endif
