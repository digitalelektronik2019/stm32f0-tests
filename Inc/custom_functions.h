#ifndef _CUSTOM_FUNCTIONS_H_
#define _CUSTOM_FUNCTIONS_H_

#include "main.h"
#include "defs.h"
#include "main.h"

/* Interrupt based frequency measurement. Makes use of structs to order things. Neato! */
void measure_frequency(Sensor *s);

/* Init function for sensor */
Sensor init_sensor();

#endif
