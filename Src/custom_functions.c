// custom_functions.c

#include "custom_functions.h"

/* Sensor init function */
Sensor init_sensor(TIM_HandleTypeDef *htim, uint32_t channel) {
	Sensor s;
	s.htim = htim;
	s.channel = channel;
	s.start = 0;
	s.end = 0;
	s.first_edge = 0;
	s.mean_freq = 0;
	s.counter = 0;
	return s;
}


/* Interrupt based frequency measurement. Makes use of structs to order things. Neato! */
void measure_frequency(Sensor *s) {

	uint32_t difference=0;

	if (s->first_edge == 0) {
		s->start = HAL_TIM_ReadCapturedValue(s->htim, s->channel);
		s->first_edge = 1;
	}
	else {
		s->end = HAL_TIM_ReadCapturedValue(s->htim, s->channel);

		if (s->end > s->start) {
			difference = s->end - s->start;
		}
		else if (s->end < s->start) {
			difference = ((0xffff - s->start) + s->end) + 1;
		}
		else {
			Error_Handler();
		}

		s->freqs[s->counter] = HAL_RCC_GetPCLK1Freq() / difference;

		if (s->counter == BUFFER-1) {
			uint32_t tmp = 0;

			for (int j=0; j<BUFFER; j++) {
				tmp += s->freqs[j];
			}
			s->mean_freq = tmp / BUFFER;
			s->counter = 0;
		}
		else {
			(s->counter)++;
		}

		s->first_edge = 0;
	}
}
